# -*- coding: utf-8 -*-
"""
Created on Wed May 24 10:19:29 2017

@author: Julia
"""
import sys
from PyQt5 import *
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
from PIL import Image
import glob
import cv2
from ui import Ui_MainWindow

class Navigate(object):
    def __init__(self):
        self.data=[]  

    def load(self):
        count = 0
        image_list = []
        
        for filename in glob.glob('C:\\Users\\Julia\\Documents\\GIT\\PupilDetectionTEST\\*.jpg'):
            image_list.append(filename)
        pixmap = QPixmap(image_list[count])
        self.label.setPixmap(pixmap)
        self.horizontalSlider.setRange(0,len(image_list)-1)
        self.horizontalSlider.sliderMoved.connect(self.sliderMoved)
    
    def sliderMoved(self, val):
        try:
            count = val
            pixmap = QPixmap(image_list[count])
            self.label.setPixmap(pixmap)
        except IndexError:
            print ("Error: No image at index"), val
            
    def keyPressEvent(self, event):
        global count
        global image_list
        key = event.key()
        if key == Qt.Key_A:
            if count > 0:
                count= count - 1 
                pixmap = QPixmap(image_list[count])
                self.label.setPixmap(pixmap)
                self.horizontalSlider.setSliderPosition(count)
    
        elif key == Qt.Key_D:
            if count < len(image_list)-1:
                count = count + 1
                pixmap = QPixmap(image_list[count])
                self.label.setPixmap(pixmap)
                self.horizontalSlider.setSliderPosition(count)
    
        elif key == Qt.Key_Escape:
            self.close()